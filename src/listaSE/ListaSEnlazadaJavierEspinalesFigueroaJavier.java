package listaSE; 
//Javier Ronaldo Espinales Figueroa
import java.io.IOException;
import javax.swing.JOptionPane;

/*
    LISTA SIMPLEMENTE ENLAZADA DE DATOS ENTEROS
*/
class CNodo {
	int dato;
	CNodo siguiente;
        
	public CNodo()	{
            siguiente = null;
	}
}

class CLista {
    
    CNodo cabeza;
    
    public CLista()
    {
            cabeza = null;
    }

    public void InsertarDato(int dat) {
        CNodo NuevoNodo;
        CNodo antes, luego;
        NuevoNodo = new CNodo();
        NuevoNodo.dato=dat;
        int ban=0;
        //hacemos una validacion 
        if (dat<0){
            
        
         if (cabeza == null){ //lista esta vacia
            NuevoNodo.siguiente=null;
            cabeza = NuevoNodo;
         }
         else {
            if (dat<cabeza.dato) //dato va antes de cabeza
            {
                    NuevoNodo.siguiente=cabeza;
                    cabeza = NuevoNodo;
            }
                else {  //dato va despues de cabeza 
                        antes=cabeza;
                        luego=cabeza;
                        while (ban==0)
                            
                        {
                            if (dat>=luego.dato) 
                            {
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==null)
                            {
                                ban=1;
                            }
                            else 
                            {
                                    if (dat<luego.dato){
                                        ban=1;
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
         }
        }
        else
        {
         JOptionPane.showMessageDialog(null, "No ha ingresado un numero negativo");
        }
    }
        
//objeto de las clases
    public void EliminarDato(int dat) {
        CNodo antes,luego;
        int ban=0;
        if (Vacia()) {
            JOptionPane.showMessageDialog(null,"Lista vacía ");
        }
        else {  if (dat<cabeza.dato) {
                    JOptionPane.showMessageDialog(null,"dato no existe en la lista ");
                }
                else {
                        if (dat==cabeza.dato) {
                            cabeza=cabeza.siguiente;
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==null) {
                                        ban=1;
                                    }
                                    else {
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (luego==null) {
                                    JOptionPane.showMessageDialog(null,"dato no existe en la Lista ");
                                }
                                else {
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            JOptionPane.showMessageDialog(null,"dato no existe en la Lista ");
                                }
                        }
                }
        }
    }
 //si es que cabeza es igual a null retorna true
    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Imprimir() {
        CNodo Temporal;
        String salida= "";
        Temporal=cabeza;
        if (!Vacia()) {
            while (Temporal!=null) {
                salida=salida+ Temporal.dato +" ";
                Temporal = Temporal.siguiente;
            }
            JOptionPane.showMessageDialog(null,salida);
        }
        else
            JOptionPane.showMessageDialog(null,"Lista vacía");
    }
}

public class ListaSEnlazadaJavierEspinalesFigueroaJavier {
    
    public static void main(String args[]) {
       
        
        /*Creamos un objeto, y con el parentesis se llaman al metodo constructor que asigna Null a cabeza
        
        System.out.println("Lista Simplemente enlazada:");
        //Ingresa un dato cuando la lista esta vacia
        objLista.InsertarDato(9);// la lista ya tiene el elemento 9 
        objLista.InsertarDato(20);//Insertar un dato mayor a los que ya están en la lista
        objLista.InsertarDato(7);
        objLista.InsertarDato(10);
        //objLista.InsertarDato(7);//Insertar un dato que ya está en la lista
        objLista.Imprimir();
        System.out.println("\nLista Sin el dato a eliminar: ");
        objLista.EliminarDato(7);
        objLista.Imprimir();*/
    

  CLista Figueroa=new CLista();
    int Javier;
  do {
      String salida="====Menú Manejo lista====\n"+"1- Insertar elemento\n"+"2- Eliminar elemento\n"+"3- Imprimir lista\n";
       salida=salida+"4- Salir\n";
       String entra=JOptionPane.showInputDialog(null, salida);
       int op = Integer.parseInt(entra);
       Javier=op;
       switch(op){
           case 1: int espinales = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero negativo"));
            Figueroa.InsertarDato(espinales);
            
            break;
            
           case 2: int espinales2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero  que desee eliminar"));
            Figueroa.EliminarDato(espinales2);
            
            break;
            
           case 3: Figueroa.Imprimir();
         
           break;
           
           case 4: System.exit(op);
           
           break;
       }
       
  }while(Javier<4);
}   
}
